class CatiyeWeb.Models.Email extends Backbone.Model
    schema:
      email:
        validators: ["required", "email"]