class CatiyeWeb.Views.EmailForm extends Backbone.Form
    template: JST['go/emailform']

    schema: 
        email:
            validators: ["required", "email"]
