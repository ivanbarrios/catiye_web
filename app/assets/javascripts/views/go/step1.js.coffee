class CatiyeWeb.Views.Step1 extends Backbone.View
    tagName: 'div'
    className: 'step1'
    template: JST['go/step1']

    events: {
        'click .btn': 'validateForm'
    }

    initialize: ->
        @form = new CatiyeWeb.Views.EmailForm(model: new CatiyeWeb.Models.Email()).render()
        @render()

    validateForm: ->
        errors = @form.commit()
        # Process validation errors
        if errors.email
            @form.$('.control-group').addClass('error')
            switch errors.email.type
              when 'required'
                @form.$('#c3_email').after('<span class="help-inline">Please enter an email address</span>')
              when 'email'
                @form.$('#c3_email').after('<span class="help-inline">Please enter a valid email address</span>')
              else
                console.log("Unexpected validation error type encountered")
            
 #            $('.step1').append("<p class=\"text-success\">Email accepted</p><p class=\"text-success\">You will receive an email. Follow the instructions in the email any time in the next 24 hours to complete account activation</p>")

    render: ->
        @$el.html @template
        $(@el).append(@form.el)
