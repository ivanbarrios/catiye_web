class CatiyeWeb.Views.HomeIndex extends Backbone.View
    template: JST['home/index']

    events: {
        'click #go': 'navigateToGo',
#        'click .btn': 'processStep1Form'
    }
    
    initialize: () ->
        @render()

    navigateToGo: ->
        # Instantiate first step in the sign-up process as a nested view under the navbar
        innerView = new CatiyeWeb.Views.Step1()
        @render(innerView)

    processStep1Form: ->
#        @$('.step1').append("<p class=\"text-success\">Email accepted</p><p class=\"text-success\">You will receive an email. Follow the instructions in the email any time in the next 24 hours to complete account activation</p>")

    render: (view) ->
        if view
            # Attach new Step 1 subview
            @$('.hero-unit').empty()
            @$('.hero-unit').append(view.el)
            # Hide irrelevant Email/Password form
            @$('.navbar-form').hide()
            # Hide bottom row content
            @$('.row').hide()
        else
            $(@el).html @template